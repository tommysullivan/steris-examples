example monorepo
================

Here we have an example monorepo. 

# Root Folder

The root folder has its own package.json. It should:

- have `private` set to `true` as the root level should never be published as a package itself; it exists to have a place to define the workspaces.

- should have a `name` that you choose - just make sure it is a unique name that is not already a taken by any package published to the package registries you are using (presumably, npmjs.org, perhaps others?)

- should have a `workspaces` array containing the file paths, relative to the project root folder, of any locally defined packages


# packages/common

Here is our common code package example, it:

- Has its own package.json
- Has a unique `name` which can be similar to or different than the file path in which it resides
- Has a `version` identifier, which can optionally follow the typical [semantically versioning](https://semver.org) convention (but which need not, until a reason to do so presents itself to the team)
- Defines a `main` file - this file is what is referred to in consuming `import` statements where the `from` refers only to the package's name (see [here](./packages/desktop-site/index.js) for example usage)
- Note that `private` is not required to be set to `true` here, so the package is publishable (if desired, although I doubt you would do that, unless to a private registry, if other teams wanted to consume your npm packages in their own code repositories). Setting it explicitly to false prevents an unintentional publishing of your package, something else you likely needn't worry about


# packages/desktop-site

We can see that this example site, which more realistically might use react, webpack, typescript and other libraries, demonstrates the simplest case of consuming shared javascript code from the common folder. It:

- Also has its own package.json
- Has its own unique name
- Requires a `version` (but you decide how diligently to maintain) 
- Expresses that the current package (desktop site) depends on the `@tommysullivan/common` package

We then can see [here](./packages/desktop-site/index.js) how we are able to consume the common function in the desktop app. 

## Testing Desktop Example

Both modern Yarn as well as modern npm support workspaces natively, and the package.jsons (and rules governing them expressed above) are expressed the same way regardless of which tool is used.

I recommend yarn because I find that:

- it is generally faster at doing its job
- calling the `scripts` defined in the various package.jsons is easier with yarn cli
- it seems more predictable / reliable to me (not a scientific measured fact)
- with yarn, one can run commands for any package from a `pwd` (current directory) that is the root of the git repository; whereas for npm, one has to cd around

### Yarn

From the root directory, run `yarn workspace @tommysullivan/desktop-site start` and you should see `hi from desktop site COMMON`

### NPM

Cd to the desktop-site and run `npm start` and you should see `hi from desktop site COMMON`


# packages/mobile-site

I set it up just like `desktop-site` but you can understand that since the package.json files are separate, desktop could use react version X with webpack and bootstrap whereas mobile may use react version with material ui. As long as the package versions in the `@tommysullivan/common` package.json don't conflict, it should be smooth sailing.


# recommended next steps

My idea, which is just my $0.02 of how to methodically and incrementally change the system while always keeping it working; as a team of course ultimately you have to decide, but here is my recommendation given what is known / unknown to me:

1. make sure that, at least for maybe this week and next, the local AND integration sites can both successfully serve the mobile AND desktop sites, both running same time, as is, no changes to package versions, no attempt to share code, just get that baseline

2. attempt to share just plain javascript in the common folder while still not changing any versions of any packages in any package.json files, using either new npm or new yarn. Make sure still that both desktop and mobile work locally, as well as deployed to integration environment before proceeding

    GOOD PRATICE: Something in your code repo should either document or automate installation of the correct, known-working npm / yarn version that you use for local and for build, so that anyone can rely on reproducing exactly what is there. 

    NOTE: If you switch to yarn, you will want to google how to properly switch while respecting the package.lock files that npm generates, which help pin your dependency versions down to precise versions, even though the package.json itself may refer to version _ranges_ that are less precise.

3. Remind yourself that when you have many possible changes in front of you, if you pick one that requires a cascade of other changes; consider whether you might first aim at a simpler change that moves you in the right direction

- let's say you set up the two packages, one mobile and one desktop, and one common, with just plain javascript being shared, and all the versions unchanged, no upgrading etc.
- can you, perhaps, before trying to share anything complex among them, aim to only upgrade, for example, react version for mobile to make it closer to desktop; change only minimum to keep it working local and in integration, no new bugs. (whichever is newer react version, desktop or mobile, move the older one towards the newest one)
- Maybe next, you try to upgrade material ui in mobile only; or even throw it out in favor of a style system that is closer to where you think you want to go
- React, material ui, these are just contrived examples, not saying to specifically do / not do those at this time
- eventually when the purposefully separate sites live in worlds that look more similar: same react version, etc. then you may come back to more interesting topics like...
- sharing a plain react component in the common package?
- sharing some javascript-only business logic in both mobile and desktop (first, decoupling the logic from syncfusion so it can be moved to common without needing common to depend on syncfusion)?
- sharing a syncfusion component itself? (at this point, more dependencies are shared and begin showing up in the package.json of the `common` package)

And then, rather than do all this in a vacuum (not that we would do that!), we can let the priority features guide our choices. Maybe the very next priority for business, after getting quick and dirty site up, is to add a search field. Maybe you are tempted to reuse what you have, but you stop and apply Occam's razor, what is simplest thing I can do to get this done? 

Sometimes, simplest / best thing will be to try and reuse code. Other times, it might be better to "play dumb" get the problem solved without worrying about reuse; and then once it is working and you can look at the solving code, and compare it to the similar code in the desktop version, "extracting out" the common parts is easier, and you have a known, working target to help guide you to extract out the right parts: no more, no less.

At some point, business may want a single url to work for mobile and desktop. Maybe that does not need to all happen at once; maybe the entrypoint url is the same but then from there, upon detecting device, user gets sent to one sub-path site or another sub-path site; and then the next week and the week after more and more urls are commonized based on priority.

Now, I know that you are not releasing to production in agile fashion. But consider that agile helps teams make generally good decisions and helps business adapt as the team learns more by working on the project. Think of Boban and the integration site as the "release" and try to release there relatively freuqently, small changes, that keep the site working. If something breaks on the integration site, don't let it linger more than few minutes; if need be, revert it back to previous working version and try to fix it. It is nice for everyone when the site is kind of kept working as you iterate.

Notice that none of what I am saying would preclude you from:

1. Deciding to create a greenfield fresh project as a new package next to the ones we start with
2. Deciding to put mobile code within the same folder hierarchy as desktop, sharing package.json, but then intelligently loading the html / javascript based on the device type.
3. Placing a load balancer or proxy or "smart index" page that temporarily aids in making the two separate websites appear mostly like one, from url perspective

So that's my recommendations for next steps.